#!/usr/bin/python3

def get_value(c):
    return {
        'a': 100,
        'b': 14,
        'c': 9,
        'd': 28,
        'e': 145,
        'f': 12,
        'g': 3,
        'h': 10,
        'i': 200,
        'j': 100,
        'k': 114,
        'l': 100,
        'm': 25,
        'n': 450,
        'o': 80,
        'p': 2,
        'q': 12,
        'r': 400,
        's': 113,
        't': 405,
        'u': 11,
        'v': 10,
        'w': 10,
        'x': 3,
        'y': 210,
        'z': 23,
        ' ': 0
    }[c]

name = input('Enter your name: ')
name = name.lower()

score = 0
for x in name:
    score += get_value(x)

if score <= 60:
    print('\nI\'m sorry to say that you\'re not too sexy!')
elif score > 60 and score <= 300:
    print('\nWell, you are pretty sexy!')
elif score > 300 and score < 600:
    print('\nWow! Very sexy!')
else:
    print('\nDamn! That burns! THE ULTIMATE SEXY!!!')

print(f'Your name scores {score} points\n\n')